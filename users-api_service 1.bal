import ballerina/http;

listener http:Listener ep0 = new (8080, config = {host: "localhost"});
# jeremia fillemon
service /vle/api/v1 on ep0 {
    resource function get users() returns User[]|http:Response {
    }
    resource function post users(@http:Payload User payload) returns CreatedInlineResponse201|http:Response {
        allusers.push(payload);
    InlineResponse201 y={userid: "1"};
    CreatedInlineResponse201 r={body:y};
    return r;
    }
    resource function get users/[string  username]() returns User|http:Response {
    }
    resource function put users/[string  username](@http:Payload User payload) returns User|http:Response {
    }
    resource function delete users/[string  username]() returns http:NoContent|http:Response {
    }
}
